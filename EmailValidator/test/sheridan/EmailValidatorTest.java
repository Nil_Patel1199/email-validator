package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import sheridan.EmailValidator;

public class EmailValidatorTest {
	
	
	@Test
	public void TestIsValidEmail( ) {
		boolean result = EmailValidator.isValidEmail("Nil@domain.co.in");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthException( ) {
		boolean result = EmailValidator.isValidEmail(". Nacdje");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthBoundaryIn( ) {
		boolean result = EmailValidator.isValidEmail("Nil.name@domain.com");
		assertFalse("Test failed",result);
	}
	
	@Test
	public void TestIsValidLengthBoundaryOut( ) {
		boolean result = EmailValidator.isValidEmail(".#dxcwsgd.com");
		assertFalse("Test failed",result);
	}
	
	@Test
	public void TestIsValidEmail1( ) {
		boolean result = EmailValidator.isValidEmail("pate1336@sheridancolleg.ca");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthException1( ) {
		boolean result = EmailValidator.isValidEmail("1w@gmail.1com");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthBoundaryIn1( ) {
		boolean result = EmailValidator.isValidEmail("daveshiva@sheridancolleg.ca");
		assertFalse("Test failed",result);
	}
	
	@Test
	public void TestIsValidLengthBoundaryOut1( ) {
		boolean result = EmailValidator.isValidEmail("11w@gmail.1com");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidEmail2( ) {
		boolean result = EmailValidator.isValidEmail("bip@srv.int");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthException2( ) {
		boolean result = EmailValidator.isValidEmail("1w@gmailnvf.1com");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthBoundaryIn2( ) {
		boolean result = EmailValidator.isValidEmail("people3jcei@sheridancolleg.ca");
		assertFalse("Test failed",result);
	}
	
	@Test
	public void TestIsValidLengthBoundaryOut2( ) {
		boolean result = EmailValidator.isValidEmail("11wgmail.1com");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthException3( ) {
		boolean result = EmailValidator.isValidEmail("1w@gmailnvf.1com");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthBoundaryIn3( ) {
		boolean result = EmailValidator.isValidEmail("people3jcei@sheridancolleg.ca");
		assertFalse("Test failed",result);
	}
	
	@Test
	public void TestIsValidLengthBoundaryOut4( ) {
		boolean result = EmailValidator.isValidEmail("11wgmail.1com");
		assertFalse("Test failed",result);
	}
}
