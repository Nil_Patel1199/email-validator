package sheridan;

public class EmailValidator {
	private static int Min_length=8;
	private static char a;
    
	
	public static boolean isValidEmail(String email )
	{
		int digit = 0;

			 for(int index = 0; index < email.length(); index++ ){   
		        a = email.charAt( index );    
		        if( Character.isDigit(a) ){
		            digit++;
		        } 
		    }  
		    if( digit >= 2 && email.matches("^[\\\\w!#$%&'*+/=?`{|}~^-]+(?:\\\\.[\\\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,6}"))
		    {
		        return true;
		    }
		   
		    else 
		    {
		    	return false;
		    }
		
	}
}
